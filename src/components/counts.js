import React from "react";
import PropTypes from "prop-types";
const Counts = props => {
  return (
    <div className="flow-right controls">
      <span>
        Item count:{" "}
        <span className={props.textcssClass} id="item-count">
          {props.itemsCount}
        </span>
      </span>
      <span>
        Unchecked count:{" "}
        <span className={props.textcssClass} id="unchecked-count">
          {props.uncheckedItemsCount}
        </span>
      </span>
    </div>
  );
};
Counts.propTypes = {
  itemsCount: PropTypes.number,
  uncheckedItemsCount: PropTypes.number
};

export default Counts;

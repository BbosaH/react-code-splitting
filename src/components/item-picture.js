import React from "react";
import PropTypes from "prop-types";

const ItemPicture = ({ pictureName }) => {
  const pictureStr = "../src/pics/" + pictureName + ".jpg";
  return (
    <img
      src={pictureStr}
      style={{ width: "50%", height: "50%" }}
      alt={pictureName}
    />
  );
};

ItemPicture.propTypes = {
  pictureName: PropTypes.string
};

export default ItemPicture;

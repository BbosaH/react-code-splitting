/* stateless component to display counts
 * Displays TodoList Items.
 */

import React from "react";
import PropTypes from "prop-types";
import TodoListItem from "./todo-list-item";

const TodoList = props => {
  return (
    <ul id="todo-list" className="todo-list">
      {props.listItems.map(item => {
        return (
          <TodoListItem
            key={item.id}
            {...item}
            checkItem={props.checkListItem}
            deleteItem={props.deleteItem}
            classNames={props.classNames}
          />
        );
      })}
    </ul>
  );
};

TodoList.propTypes = {
  listItems: PropTypes.array,
  checkListItem: PropTypes.func,
  deleteItem: PropTypes.func
};

export default TodoList;

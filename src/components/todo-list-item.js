/* stateless component to create TodolistItem counts
 * Displays TodoListItem consisting of Checkbox , Text and Delete Button to delete List Item
 */

import React from "react";
import PropTypes from "prop-types";
import Checkbox from "./checkbox";
import ItemPicture from "./item-picture";

const TodoListItem = props => {
  console.log(props.text);
  return (
    <React.Fragment>
      <li className={props.classNames.TODO_ITEM}>
        <Checkbox
          name={props.id}
          checked={props.checked}
          onChange={props.checkItem}
          inputcssClass={props.classNames.TODO_CHECKBOX}
        />
        {props.checked ? (
          <strike className={props.classNames.TODO_CROSSED}>
            {props.text}
          </strike>
        ) : (
          <span className={props.classNames.TODO_TEXT}>{props.text}</span>
        )}

        <button
          className={props.classNames.TODO_DELETE}
          name={props.id}
          onClick={props.deleteItem}
        >
          delete
        </button>
      </li>
      <div>
        {/tito$/.test(props.text) || /bran$/.test(props.text) ? (
          <ItemPicture
            pictureName={props.text.trim().substring(props.text.length - 4)}
          />
        ) : null}
      </div>
    </React.Fragment>
  );
};

TodoListItem.propTypes = {
  id: PropTypes.number,
  checked: PropTypes.bool,
  checkItem: PropTypes.func,
  text: PropTypes.string,
  deleteItem: PropTypes.func
};

export default TodoListItem;

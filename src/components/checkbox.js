/* stateless component to create CheckBox Widget
 */
import React from "react";
import PropTypes from "prop-types";

const Checkbox = ({
  type = "checkbox",
  name,
  checked = false,
  onChange,
  inputcssClass
}) => (
  <input
    className={inputcssClass}
    type={type}
    name={name}
    checked={checked}
    onChange={onChange}
  />
);

Checkbox.propTypes = {
  onChange: PropTypes.func,
  checked: PropTypes.bool,
  name: PropTypes.number
};

export default Checkbox;

/* stateless component to create New Todo
 * contains text input to type TOdo Task and Button To add the New Todo Task
 */
import React from "react";
import PropTypes from "prop-types";

const CreateNewTodo = props => {
  return (
    <React.Fragment>
      <input
        className="center"
        type="text"
        onChange={props.change}
        value={props.todoName}
      />
      <button
        className="button center"
        onClick={props.clickButton}
        style={{ marginTop: 10 }}
      >
        New TODO
      </button>
    </React.Fragment>
  );
};
CreateNewTodo.propTypes = {
  change: PropTypes.func,
  todoName: PropTypes.string,
  clickButton: PropTypes.func
};

export default CreateNewTodo;

import React, { Suspense, lazy } from "react";
import "./styles.css";
import Counts from "./counts";
import CreateNewTodo from "./create-new-todo";
//import TodoList from "./todo-list";
//import SimpleMap from "./simple-map";
const TodoList = lazy(() =>
  import(/* webpackChunkName : "todo-list",webpackMode: "lazy"*/ "./todo-list")
);

const SimpleMap = lazy(() =>
  import(
    /* webpackChunkName : "locationmap",webpackMode: "lazy"*/ "./simple-map"
  )
);

const classNames = {
  TODO_ITEM: "todo-container",
  TODO_CHECKBOX: "todo-checkbox",
  TODO_TEXT: "todo-text",
  TODO_DELETE: "todo-delete",
  TODO_CROSSED: "todo-crossed"
};

/* One classComponent that manages state of the App. component to display counts
 * Displays count  Of Todo Items & count of Unchecked todo Items.
 */
class TodoApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentinputVal: "",
      listItems: [],
      showMapToggle: false
    };
    this.setCurrentVal = this.setCurrentVal.bind(this);
    this.addNewTodo = this.addNewTodo.bind(this);
    this.checkItem = this.checkItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.toggleMap = this.toggleMap.bind(this);
  }

  //Method to update input value from input
  setCurrentVal(event) {
    let val = event.target.value;
    this.setState(prevState => {
      return {
        currentinputVal: val
      };
    });
  }
  //Method to add new Todo Item to list
  addNewTodo() {
    if (this.state.currentinputVal) {
      this.setState(prevState => {
        return {
          listItems: [
            {
              id: Date.now(),
              text: prevState.currentinputVal,
              checked: false
            },
            ...prevState.listItems
          ],
          currentinputVal: ""
        };
      });
    }
  }
  //Method to toggle checking of Items on a list
  checkItem(event) {
    const id = event.target.name;
    let item = this.state.listItems.find(item => {
      return item.id === parseInt(id);
    });
    item.checked = !item.checked;
    this.setState(prevState => {
      return {
        ...prevState.listItems,
        item
      };
    });
  }

  //method to delete item from list
  deleteItem(e) {
    const id = event.target.name;
    console.log(id);
    let itemIndex = this.state.listItems.findIndex(item => {
      return item.id === parseInt(id);
    });
    this.setState(prevState => {
      return {
        ...prevState.listItems,
        ...prevState.listItems.splice(itemIndex, 1)
      };
    });
  }
  toggleMap() {
    this.setState(prevState => {
      return {
        showMapToggle: !prevState.showMapToggle
      };
    });
  }

  showMap() {
    return (
      <Suspense fallback={<h1>Still Loading…</h1>}>
        <SimpleMap />
      </Suspense>
    );
  }

  render() {
    return (
      <React.Fragment>
        <h1 className="center title">My Map and Todo App</h1>
        <button
          className={classNames.TODO_DELETE}
          name="locate"
          onClick={this.toggleMap}
        >
          Show Location
        </button>
        {this.state.showMapToggle ? this.showMap() : null}
        <br />
        <br />
        <br />
        <Counts
          itemsCount={this.state.listItems.length}
          uncheckedItemsCount={
            this.state.listItems.filter(item => {
              return !item.checked;
            }).length
          }
          textCssClass={classNames.TODO_TEXT}
        />
        <CreateNewTodo
          change={this.setCurrentVal}
          todoName={this.state.currentinputVal}
          clickButton={this.addNewTodo}
        />
        {this.state.listItems.length > 0 ? (
          <Suspense fallback={<h1>Still Loading…</h1>}>
            <TodoList
              listItems={this.state.listItems}
              checkListItem={this.checkItem}
              deleteItem={this.deleteItem}
              classNames={classNames}
            />
          </Suspense>
        ) : null}
        {/* <TodoList
          listItems={this.state.listItems}
          checkListItem={this.checkItem}
          deleteItem={this.deleteItem}
          classNames={classNames}
        /> */}
      </React.Fragment>
    );
  }
}
export default TodoApp;
